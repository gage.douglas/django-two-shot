from django import forms


class AccountForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(attrs={'thingy': 'Password'})
    )


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150)
    password_confirmation = forms.CharField(max_length=150,
                                            widget=forms.PasswordInput)
